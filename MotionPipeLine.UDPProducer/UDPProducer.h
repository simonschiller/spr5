#pragma once

#include "../MotionPipeline.Core/Publisher.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include "udp_socket.h"
#include <string>

using namespace std;
class UDPProducer : public Publisher
{
public:
	UDPProducer(int port,MessageBus* message_bus);
	~UDPProducer();
	virtual void update() override;
private:
	wsa_session m_session;
	udp_socket* m_socket_server;
	int id;
};

