#include "pch.h"
#include "UDPProducer.h"
#include <iostream>

UDPProducer::UDPProducer(int port, MessageBus* message_bus): Publisher (message_bus)
{
	m_session = wsa_session();
	m_socket_server = new udp_socket();
	if (m_socket_server->bind(port))
		cout << "Binding to port: " << port << " - Success" << endl;
	else
		cout << "Binding to port: " << port << " - Failure" << endl;

	id = reinterpret_cast<int>(&m_socket_server); // Use memory address of the socket server as unique id
}

UDPProducer::~UDPProducer()
{
	delete m_socket_server;
}

void UDPProducer::update()
{
	string message;
	m_socket_server->receive_message(message);
	if (message.length() > 0)
	{
		cout << "UDP Publisher sending " << message << endl;
		//MessageFormat = "label=value;x=value;y=value;z=value"
		double x = 0;
		double y = 0;
		double z = 0;
		string label;
		std::string delimiter = ";";
		size_t pos = 0;
		std::string token;
		while ((pos = message.find(delimiter)) != std::string::npos) {
			token = message.substr(0, pos);

			string subdelimitier = "=";
			int curPos = token.find(subdelimitier);
			string subtoken = token.substr(0, curPos);
			token.erase(0, curPos + subdelimitier.length());
			if (subtoken == "label")
				label = token;
			else if (subtoken == "x")
				x = atof(token.c_str());
			else if (subtoken == "y")
				y = atof(token.c_str());
			else if (subtoken == "z")
				z = atof(token.c_str());

			message.erase(0, pos + delimiter.length());
		}

		send(new CoordinateMessage(label, id, x, y, z));
	}
}
