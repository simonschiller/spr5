#pragma once
#include "pch.h"
#include "HandFunctions.h"


using namespace std;

//Extracts the positions of the given finger and adds them to the messages
//IN messages: The the message queue
//IN finger: The finger, of which the positions should be extracted
//IN hand_side: Describes which hand it is (left or right)
//IN sender_id: Unique id used to identify the sender of this message
void HandUtil::extract_finger_positions_and_add_to_messages(vector<CoordinateMessage*>& messages, LEAP_DIGIT& finger, Hand& hand_side, int sender_id) {
	string current_finger = "";

	//Check which finger it is
	switch (finger.finger_id) {
	case 0: //THUMB
		current_finger = "thumb";
		break;
	case 1: //INDEX
		current_finger = "index finger";
		break;
	case 2: //MIDDLE
		current_finger = "middle finger";
		break;
	case 3: //RING
		current_finger = "ring finger";
		break;
	case 4: //PINKY
		current_finger = "pinky";
		break;
	default:
		current_finger = "undefined";
		break;
	}

	if (current_finger != "undefined") {
		//Define the base label for the current finger
		string base_label = hand_side == Hand::LEFT_HAND ? "left " + current_finger : "right " + current_finger;

		//Get the individual positions of the current finger
		//DISTAL -> The top part of the finger
		//INTERMEDIATE -> The middle part of the finger
		//PROXIMAL -> The bottom part of the finger
		LEAP_VECTOR tip = finger.distal.next_joint;
		LEAP_VECTOR base = finger.proximal.prev_joint;

		if (current_finger == "thumb") {  //If the current_finger is the thumb, the proximal bone would actually be the metacarpals (bones in the palm) -> Therefore get the intermediate bone for the thumb
			base = finger.intermediate.prev_joint;
		}

		//Add the positions to the messages.
		messages.push_back(new CoordinateMessage(base_label + " tip", sender_id, tip.x, tip.y, tip.z));
		messages.push_back(new CoordinateMessage(base_label + " base", sender_id, base.x, base.y, base.z));
	}
}

//Extracts the palm position and adds it to the messages
//IN messages: The the message queue
//IN hand: The hand, of which the palm positions should be extracted
//IN hand_side: Describes which hand it is (left or right)
//IN sender_id: Unique id used to identify the sender of this message
void HandUtil::extract_palm_position_and_add_to_messages(vector<CoordinateMessage*>& messages, LEAP_HAND& hand, Hand& hand_side, int sender_id) {
	string base_label = hand_side == Hand::LEFT_HAND ? "left" : "right";
	messages.push_back(new CoordinateMessage(base_label + " hand palm", sender_id, hand.palm.position.x, hand.palm.position.y, hand.palm.position.z));
}