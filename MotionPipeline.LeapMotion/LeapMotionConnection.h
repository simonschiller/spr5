#pragma once

#include "LeapC.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"

#include <mutex>
#include <vector>

using namespace std;

class LeapMotionConnection {
	static LEAP_CONNECTION connection_handle;
	static bool is_connected;
	static volatile bool is_running;

	// To support polling
	static LEAP_TRACKING_EVENT* last_frame;
	static void cache_frame(const LEAP_TRACKING_EVENT* frame);

	// Threading
	static void handle_leap_event();
	static mutex frame_mutex;

public:
	LeapMotionConnection();
	void wait_until_connected();

	vector<CoordinateMessage*> get_current_state(int sender_id);
};
