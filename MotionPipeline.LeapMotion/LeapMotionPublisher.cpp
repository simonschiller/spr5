#include "pch.h"
#include "LeapMotionPublisher.h"

// Macro to generate random coordinates in debug mode
#ifdef _DEBUG
#define RAND_COORD ((double) rand() / RAND_MAX * 200 - 100)
#endif

LeapMotionPublisher::LeapMotionPublisher(MessageBus* message_bus) : Publisher(message_bus) {
	id = reinterpret_cast<int>(&connection); // Use the memory address of the connection as unique id
	connection.wait_until_connected();
}

void LeapMotionPublisher::update()
{
	vector<CoordinateMessage*> state = connection.get_current_state(id);

	// Send all messages
	for (auto message : state) {
		send(message);
	}

	// If compiled in debug mode, simulate values when no input from the LeapMotion has been received
#ifdef _DEBUG
	if (state.empty()) {
		send(new CoordinateMessage("left hand palm", id, RAND_COORD, RAND_COORD, RAND_COORD));
	}
#endif
}
