#include "pch.h"
#include "LeapMotionConnection.h"
#include "HandFunctions.h"

#include <chrono>
#include <exception>
#include <iostream>
#include <thread>

using namespace std;

// Initialize static members
LEAP_CONNECTION LeapMotionConnection::connection_handle = nullptr;
bool LeapMotionConnection::is_connected = false;
volatile bool LeapMotionConnection::is_running = false;
LEAP_TRACKING_EVENT* LeapMotionConnection::last_frame = nullptr;
mutex LeapMotionConnection::frame_mutex;

// Constructor, establish the connection to the Leap Motion device
LeapMotionConnection::LeapMotionConnection() {

	// Do nothing if the connection is already established
	if (is_running) {
		return;
	}

	// Create a connection to the Leap Motion device
	if (connection_handle || LeapCreateConnection(nullptr, &connection_handle) == eLeapRS_Success) {
		eLeapRS result = LeapOpenConnection(connection_handle); // Open the connection

		// Throw an exception if the connection could not be established
		if (result != eLeapRS_Success) {
			throw exception("Could not establish connection");
		}
		is_running = true;

		// Start the worker thread that processes the frame events
		thread worker_thread(&LeapMotionConnection::handle_leap_event);
		worker_thread.detach();
	}
}

void LeapMotionConnection::handle_leap_event() {
	eLeapRS result;
	LEAP_CONNECTION_MESSAGE message;

	// Loop until the connection is not running anymore
	while (is_running) {
		unsigned int timeout = 1000;
		result = LeapPollConnection(connection_handle, timeout, &message);

		// Make sure polling is successful
		if (result != eLeapRS_Success) {
			throw exception("Polling connection failed!");
		}

		// Keep track of the connection state
		if (message.type == eLeapEventType_Connection) {
			is_connected = true;
		}
		else if (message.type == eLeapEventType_ConnectionLost) {
			is_connected = false;
		}

		// If a new frame has been received, save it
		if (message.type == eLeapEventType_Tracking) {
			cache_frame(message.tracking_event);
		}
	}
}

void LeapMotionConnection::wait_until_connected() {
	while (!is_connected) {
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

vector<CoordinateMessage*> LeapMotionConnection::get_current_state(int sender_id) {
	frame_mutex.lock();

	// Return an empty vector if there is no cached frame
	if (last_frame == nullptr) {
		frame_mutex.unlock();
		return vector<CoordinateMessage*>();
	}
	LEAP_TRACKING_EVENT frame = *last_frame;
	frame_mutex.unlock();

	vector<CoordinateMessage*> messages;
	Hand hand_side;

	// Convert the frame into a message
	for (uint32_t i = 0; i < frame.nHands; i++) {
		LEAP_HAND* hand = &frame.pHands[i];
		
		hand_side = hand->type == eLeapHandType_Left ? Hand::LEFT_HAND : Hand::RIGHT_HAND;
		
		// Convert the palm position into a message
		HandUtil::extract_palm_position_and_add_to_messages(messages, *hand, hand_side, sender_id);

		//Convert all positions of each individual finger into positions and add them to the messages
		HandUtil::extract_finger_positions_and_add_to_messages(messages, hand->thumb, hand_side, sender_id);
		HandUtil::extract_finger_positions_and_add_to_messages(messages, hand->index, hand_side, sender_id);
		HandUtil::extract_finger_positions_and_add_to_messages(messages, hand->middle, hand_side, sender_id);
		HandUtil::extract_finger_positions_and_add_to_messages(messages, hand->ring, hand_side, sender_id);
		HandUtil::extract_finger_positions_and_add_to_messages(messages, hand->pinky, hand_side, sender_id);
	}

	return messages;
}

void LeapMotionConnection::cache_frame(const LEAP_TRACKING_EVENT* frame) {
	lock_guard<mutex> guard(frame_mutex);
	if (last_frame == nullptr) {
		last_frame = new LEAP_TRACKING_EVENT;
	}
	*last_frame = *frame;
}
