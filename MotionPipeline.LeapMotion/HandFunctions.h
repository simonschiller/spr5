#pragma once
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include "LeapC.h"
#include <vector>

//Describes the side of the hand -> either the left or the right hand
enum class Hand {
	LEFT_HAND,
	RIGHT_HAND
};

class HandUtil {
public:

	HandUtil() = delete;
	~HandUtil() = delete;

	//Extracts the positions of the given finger and adds them to the messages
	//IN messages: The the message queue
	//IN finger: The finger, of which the positions should be extracted
	//IN hand_side: Describes which hand it is (left or right)
	//IN sender_id: Unique id used to identify the sender of this message
	static void extract_finger_positions_and_add_to_messages(std::vector<CoordinateMessage*>& messages, LEAP_DIGIT& finger, Hand& hand_side, int sender_id);

	//Extracts the palm position and adds it to the messages
	//IN messages: The the message queue
	//IN hand: The hand, of which the palm positions should be extracted
	//IN hand_side: Describes which hand it is (left or right)
	//IN sender_id: Unique id used to identify the sender of this message
	static void extract_palm_position_and_add_to_messages(std::vector<CoordinateMessage*>& messages, LEAP_HAND& hand, Hand& hand_side, int sender_id);
};

