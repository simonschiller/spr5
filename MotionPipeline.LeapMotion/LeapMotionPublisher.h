#pragma once

#include "LeapMotionConnection.h"
#include "../MotionPipeline.Core/Publisher.h"
#include "../MotionPipeline.Core/MessageBus.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include "LeapC.h"

class LeapMotionPublisher : public Publisher
{
	LeapMotionConnection connection;
	int id;

public:
	LeapMotionPublisher(MessageBus* message_bus);

	virtual void update() override;
};

