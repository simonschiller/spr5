///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2017, Carnegie Mellon University and University of Cambridge,
// all rights reserved.
//
// ACADEMIC OR NON-PROFIT ORGANIZATION NONCOMMERCIAL RESEARCH USE ONLY
//
// BY USING OR DOWNLOADING THE SOFTWARE, YOU ARE AGREEING TO THE TERMS OF THIS LICENSE AGREEMENT.
// IF YOU DO NOT AGREE WITH THESE TERMS, YOU MAY NOT USE OR DOWNLOAD THE SOFTWARE.
//
// License can be found in OpenFace-license.txt

//     * Any publications arising from the use of this software, including but
//       not limited to academic journal and conference publications, technical
//       reports and manuals, must cite at least one of the following works:
//
//       OpenFace 2.0: Facial Behavior Analysis Toolkit
//       Tadas Baltru�aitis, Amir Zadeh, Yao Chong Lim, and Louis-Philippe Morency
//       in IEEE International Conference on Automatic Face and Gesture Recognition, 2018
//
//       Convolutional experts constrained local model for facial landmark detection.
//       A. Zadeh, T. Baltru�aitis, and Louis-Philippe Morency,
//       in Computer Vision and Pattern Recognition Workshops, 2017.
//
//       Rendering of Eyes for Eye-Shape Registration and Gaze Estimation
//       Erroll Wood, Tadas Baltru�aitis, Xucong Zhang, Yusuke Sugano, Peter Robinson, and Andreas Bulling
//       in IEEE International. Conference on Computer Vision (ICCV),  2015
//
//       Cross-dataset learning and person-specific normalisation for automatic Action Unit detection
//       Tadas Baltru�aitis, Marwa Mahmoud, and Peter Robinson
//       in Facial Expression Recognition and Analysis Challenge,
//       IEEE International Conference on Automatic Face and Gesture Recognition, 2015
//
///////////////////////////////////////////////////////////////////////////////
// FaceTrackingVid.cpp : Defines the entry point for the console application for tracking faces in videos.

// Libraries for landmark detection (includes CLNF and CLM modules)
#include "pch.h"
#include "OpenFacePublisher.h"
#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"
#include "FaceAnalyser.h"

#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>

// framework references
#ifdef _WIN32
#include <windows.h>    //GetModuleFileNameW
#else
#include <limits.h>
#include <unistd.h>     //readlink
#endif
#include <thread>

using namespace std;
using namespace filesystem;

#define INFO_STREAM( stream ) \
std::cout << stream << std::endl

#define WARN_STREAM( stream ) \
std::cout << "Warning: " << stream << std::endl

#define ERROR_STREAM( stream ) \
std::cout << "Error: " << stream << std::endl

// Macro to generate random coordinates in debug mode
#ifdef _DEBUG
#define RAND_COORD ((double) rand() / RAND_MAX * 200 - 100)
#endif

//members
std::thread background_thread;
std::vector<CoordinateMessage*> message_buffer = std::vector<CoordinateMessage*>();
std::mutex mtx;

std::filesystem::path getexepath()
{
#ifdef _WIN32
	wchar_t path[MAX_PATH] = { 0 };
	GetModuleFileNameW(NULL, path, MAX_PATH);
	return path;
#else
	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	return std::string(result, (count > 0) ? count : 0);
#endif
}

void run_in_background(std::vector<CoordinateMessage*>& message_buffer)
{
	std::vector<std::string> arguments;
	auto dir = getexepath();

	arguments.push_back(dir.string()); // get path of current executable
	arguments.push_back("-device"); // selected input device
	arguments.push_back("0"); // first

	LandmarkDetector::FaceModelParameters det_parameters(arguments);

	// The modules that are being used for tracking
	LandmarkDetector::CLNF face_model(det_parameters.model_location);
	if (!face_model.loaded_successfully)
	{
		std::cout << "ERROR: Could not load the landmark detector" << std::endl;
	}

	if (!face_model.eye_model)
	{
		std::cout << "WARNING: no eye model found" << std::endl;
	}

	// Open a sequence
	Utilities::SequenceCapture sequence_reader;

	// A utility for visualizing the results (show just the tracks)
	Utilities::Visualizer visualizer(true, false, false, false);

	// Tracking FPS for visualization
	Utilities::FpsTracker fps_tracker;
	fps_tracker.AddFrame();

	// Load facial feature extractor and AU analyser
	FaceAnalysis::FaceAnalyserParameters face_analysis_params(arguments);
	face_analysis_params.OptimizeForVideos();
	FaceAnalysis::FaceAnalyser face_analyser(face_analysis_params);

	int sequence_number = 0;
	int id = 42;

	while (true) // this is not a for loop as we might also be reading from a webcam
	{
		// The sequence reader chooses what to open based on command line arguments provided
		if (!sequence_reader.Open(arguments))
			break;

		INFO_STREAM("Device or file opened");

		cv::Mat rgb_image = sequence_reader.GetNextFrame();

		INFO_STREAM("Starting tracking");
		while (!rgb_image.empty()) // this is not a for loop as we might also be reading from a webcam
		{
			// Reading the images
			cv::Mat_<uchar> grayscale_image = sequence_reader.GetGrayFrame();

			// The actual facial landmark detection / tracking
			bool detection_success = LandmarkDetector::DetectLandmarksInVideo(rgb_image, face_model, det_parameters, grayscale_image);

			// Gaze tracking, absolute gaze direction
			cv::Point3f gazeDirection0(0, 0, -1);
			cv::Point3f gazeDirection1(0, 0, -1);

			// If tracking succeeded and we have an eye model, estimate gaze
			if (detection_success && face_model.eye_model)
			{
				GazeAnalysis::EstimateGaze(face_model, gazeDirection0, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, true);
				GazeAnalysis::EstimateGaze(face_model, gazeDirection1, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, false);

				mtx.lock();
				message_buffer.push_back(new CoordinateMessage("gaze_left", id, gazeDirection0.x, gazeDirection0.y, gazeDirection0.z));
				message_buffer.push_back(new CoordinateMessage("gaze_right", id, gazeDirection1.x, gazeDirection1.y, gazeDirection1.z));
				mtx.unlock();

				face_analyser.PredictStaticAUsAndComputeFeatures(rgb_image, face_model.detected_landmarks);

				//facial expressions (AU's)
				auto aus_intensity = face_analyser.GetCurrentAUsReg();
				//auto aus_presence = face_analyser.GetCurrentAUsClass();

				for (auto& intensity : aus_intensity)
				{
					string label = "";

					if (intensity.first == "AU01") {
						label = "inner_brow_raiser";
					}
					else if (intensity.first == "AU02") {
						label = "outer_brow_raiser";
					}
					else if (intensity.first == "AU04") {
						label = "brow_lowerer";
					}
					else if (intensity.first == "AU12") {
						label = "lip_corner_puller";
					}
					else if (intensity.first == "AU17") {
						label = "chin_raiser";
					}
					else if (intensity.first == "AU25") {
						label = "lips_part";
					}
					else if (intensity.first == "AU26") {
						label = "jaw_drop";
					}

					if (intensity.first == "AU01" //inner brow raiser
						|| intensity.first == "AU02" //outer brow raiser
						|| intensity.first == "AU04" //brow lowerer
						|| intensity.first == "AU12" //Lip corner puller -> nearest to a smile -> works pretty good to detect a smile
						|| intensity.first == "AU17" //chin raiser -> works best for kiss mouth
						|| intensity.first == "AU25" //lips part
						|| intensity.first == "AU26" //jawdrop -> in combination with AU 25 the open mouth could be detected the best via facial expressions
						)
					{
						mtx.lock();
						message_buffer.push_back(new CoordinateMessage(label, id, intensity.second, 0, 0));
						mtx.unlock();
					}
				}
			}

			// Work out the pose of the head from the tracked model
			cv::Vec6d pose_estimate = LandmarkDetector::GetPose(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
			mtx.lock();															//X - Coordinate   Y-Coordinate     Z-Coordinate  
			message_buffer.push_back(new CoordinateMessage("pose_estimate", id, pose_estimate[0], pose_estimate[1], pose_estimate[2]));
			mtx.unlock();

			// Keeping track of FPS
			fps_tracker.AddFrame();

			// Displaying the tracking visualizations
			visualizer.SetImage(rgb_image, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
			visualizer.SetObservationLandmarks(face_model.detected_landmarks, face_model.detection_certainty, face_model.GetVisibilities());
			visualizer.SetObservationPose(pose_estimate, face_model.detection_certainty);
			visualizer.SetObservationGaze(gazeDirection0, gazeDirection1, LandmarkDetector::CalculateAllEyeLandmarks(face_model), LandmarkDetector::Calculate3DEyeLandmarks(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy), face_model.detection_certainty);
			visualizer.SetFps(fps_tracker.GetFPS());
			// detect key presses (due to pecularities of OpenCV, you can get it when displaying images)
			char character_press = visualizer.ShowObservation();

			// Grabbing the next frame in the sequence
			rgb_image = sequence_reader.GetNextFrame();
		}

		// Reset the model, for the next video
		face_model.Reset();
		sequence_reader.Close();

		sequence_number++;
	}
}


OpenFacePublisher::OpenFacePublisher(MessageBus* message_bus) : Publisher(message_bus) {
	background_thread = std::thread(run_in_background, std::ref(message_buffer));
}

void OpenFacePublisher::update()
{
	mtx.lock();
	while (!message_buffer.empty())
	{
		auto message = message_buffer.back();
		send(message);
		message_buffer.pop_back();
	}
	mtx.unlock();
}