#pragma once

#include "../MotionPipeline.Core/Publisher.h"
#include "../MotionPipeline.Core/MessageBus.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include <vector>

class OpenFacePublisher : public Publisher
{
public:
	OpenFacePublisher(MessageBus* message_bus);

	virtual void update() override;
};
