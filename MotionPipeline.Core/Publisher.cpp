#include "pch.h"
#include "Publisher.h"


Publisher::Publisher(MessageBus* messageBus) : message_bus(messageBus)
{
	this->message_bus->register_publisher([this]() -> void { this->update(); });
}

void Publisher::send(Message* message)
{
	message_bus->send(message);
}