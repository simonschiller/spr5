#pragma once

#include "MessageBus.h"

/*
Publishes Messages thru the MessageBus
*/
class Publisher
{
private:
	MessageBus *message_bus; //back reference to the message bus

public:
	Publisher(MessageBus* messageBus);

	/*
		Update method to be triggered by the message bus, do your pull logic here
	*/
	virtual void update() = 0;

	/*
		Send a new message to the message bus
	*/
	virtual void send(Message* message);
};