#pragma once

#include <string>

class Message
{
	std::string label;
	int sender_id;

public:
	Message(const std::string& label, int sender_id) : label(label), sender_id(sender_id) {};
	virtual ~Message() {}; // To avoid memory leaks when a derived class is deleted

	inline std::string get_label() const { return label; };
	inline int get_sender_id() const { return sender_id; };
};
