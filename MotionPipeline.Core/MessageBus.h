#pragma once

#include "Message.h"
#include <functional>
#include <queue>
#include <iostream>
#include <unordered_map>

/*
Handle Communication between Publishers and Subscribers
*/
class MessageBus
{
public:

	/*
		Register a publisher to be able to notify all subscribers about a new message
	*/
	void register_publisher(std::function<void()> publisher);

	/*
		Register a subscriber to be able to get notified about a new message
	*/
	void register_subscriber(std::function<void(Message*)>  subscriber);

	/*
		Start the communication cycle
	*/
	void run();

	/*
		Add a new message to be sent to all subscribers
	*/
	void send(Message* message);

	/*
		Inform subscribers about the new messages
	*/
	void notify();

	/*
		Stop the running publisher
	*/
	void stop();

private:
	bool is_running;

	std::vector<std::function<void()>> publishers; //function to update publishers
	std::vector<std::function<void(Message*)>> subscribers; //function to notify subscribers

	std::queue<Message*> messages; //message queue

	std::unordered_map<std::string, long long> timestamps;
	const long long DEBOUNCE_INTERVAL = 1000000000 / 60; // Allow 60 messages per second
};