#include "pch.h"
#include "Subscriber.h"

Subscriber::Subscriber(MessageBus* messageBus) : message_bus(messageBus)
{
	this->message_bus->register_subscriber([this](Message* message) -> void {
		this->on_notify(message);
	});
}
