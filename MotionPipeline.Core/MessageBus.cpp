// MessageBus.cpp : Defines the functions for the static library.

#include "pch.h"
#include "framework.h"
#include "MessageBus.h"
#include <chrono>
#include <cstdint>

void MessageBus::register_publisher(std::function<void()> publisher)
{
	publishers.push_back(publisher);
}

void MessageBus::register_subscriber(std::function<void(Message*)> subscriber)
{
	subscribers.push_back(subscriber);
}

void MessageBus::run()
{
	is_running = true;

	while (is_running) // TODO: make more reliable
	{
		// Iterate through all the publishers and tell them to update their interal status
		for (const auto& publisher : publishers) {
			publisher();
		}

		// Inform all subscribers about new messages
		notify();
	}
}

void MessageBus::send(Message* message)
{
	long long now = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	long long last_sent = timestamps[message->get_label()];

	// Debounce messages to not spam consumers
	if (now >= last_sent + DEBOUNCE_INTERVAL)
	{
		messages.push(message);
		timestamps[message->get_label()] = now;
	}
}

void MessageBus::notify()
{
	while (!messages.empty()) {
		for (const auto& subscriber : subscribers) {
			subscriber(messages.front());
		}
		delete messages.front(); // Free the allocated memory
		messages.pop();
	}
}

void MessageBus::stop()
{
	if (!is_running)
	{
		std::cout << "Tried to stop not running MessageBus." << std::endl;
	}

	is_running = false;
}