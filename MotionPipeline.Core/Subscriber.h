#pragma once

#include "MessageBus.h"

/*
Subscribes to messages from the message bus
*/
class Subscriber
{
protected:
	MessageBus* message_bus; //back reference to the message bus

public:
	Subscriber(MessageBus* messageBus);

	/*
		Get notified by the message bus
	*/
	virtual void on_notify(Message* message) = 0;
};