#pragma once

#include "..//MotionPipeline.Core/Message.h"
#include <string>
#include <ostream>

class CoordinateMessage : public Message
{
	double x, y, z;

public:
	CoordinateMessage(std::string label, int sender_id, double x, double y, double z)
		: Message(label, sender_id), x(x), y(y), z(z) {}

	double get_x() const { return x; };
	double get_y() const { return y; };
	double get_z() const { return z; };

	friend std::ostream& operator<<(std::ostream& out, const CoordinateMessage& m) {
		out << m.get_label() << " event tracked " << m.get_label() << " at (";
		return out << m.x << ", " << m.y << ", " << m.z << ")";
	}
};