#pragma once
#include "Coordinate.h"

#define MAX_INPUT_VALUE 1000
#define MAX_OUTPUT_VALUE 255

class Transformation
{
public:
	/*ctor*/Transformation();
	/*ctor*/Transformation(int max_input_, int max_output_);
	/*ctor*/Transformation(int max_input_, int max_output_, int min_input_, int min_output_);
	double transform_coordinates_to_axis_position(Coordinate& coordinate);
private:

	double input_range;
	double output_range;
};

