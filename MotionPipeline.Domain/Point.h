#pragma once
#include "Axis.h"
#include "Coordinate.h"

class Point
{
public:
	Point(Axis& axis_);
	virtual ~Point();

	void set_new_position(Coordinate& coord);

private:

	Axis& axis;
	Coordinate act_pos;
	Coordinate old_pos;
	Point* relative_point;
};

