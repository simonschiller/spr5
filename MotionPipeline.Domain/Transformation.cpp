#include "Transformation.h"

Transformation::Transformation()
	:input_range(MAX_INPUT_VALUE)
	, output_range(MAX_OUTPUT_VALUE)
{}

Transformation::Transformation(int max_input_, int max_output_)
	: input_range(max_input_)
	, output_range(max_output_)
{}
Transformation::Transformation(int max_input_, int max_output_, int min_input_, int min_output_)
	: input_range(((double)max_input_ - (double)min_input_))
	, output_range(((double)max_output_ - (double)min_output_))
{}

double Transformation::transform_coordinates_to_axis_position(Coordinate& coordinate)
{
	double x = coordinate.get_x();
	return (x / input_range) * output_range;
}
