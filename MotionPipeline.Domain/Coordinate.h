#pragma once

class Coordinate
{
public:
	Coordinate();
	virtual ~Coordinate();
	double get_x();
	double get_y();
	double get_z();
	void set_coordinate(double x_, double y_, double z_);

private:
	double x;
	double y;
	double z;
};

