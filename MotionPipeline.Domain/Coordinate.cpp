#include "pch.h"
#include "Coordinate.h"

Coordinate::Coordinate()
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

Coordinate::~Coordinate()
{
}

double Coordinate::get_x()
{
	return x;
}

double Coordinate::get_y()
{
	return y;
}

double Coordinate::get_z()
{
	return z;
}

void Coordinate::set_coordinate(double x_, double y_, double z_)
{
	x = x_;
	y = y_;
	z = z_;
}
