#pragma once
#include "Transformation.h"
#include "Limits.h"

class Axis
{
public:
	Axis(Transformation& transformation_, Limits& limits_);
	/*transforms the relative coordinate change into a position movement on the axis and executes this */
	/* relativeCoordinate: relative movement in coordinate */
	/*returns true if execution was successfull, false if an error occurred */
	bool move(Coordinate& relativeCoordinate);

	double get_act_position();
	
private:
	Transformation& transform;
	Limits& limits;
	double act_position;
	double old_position;
};

