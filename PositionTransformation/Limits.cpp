#include "Limits.h"

Limits::Limits()
: velocity(100.0)
, acceleration(1000.0)
, deceleration(1000.0)
, positive_position(255.0)
, negative_position(-255.0)
{
}

Limits::Limits(double posPos, double negPos)
: velocity(100.0)
, acceleration(1000.0)
, deceleration(1000.0)
, positive_position(posPos)
, negative_position(negPos)
{
}

Limits::Limits(double vel, double acc, double dec, double pos_pos, double neg_pos)
	: velocity(vel)
	, acceleration(acc)
	, deceleration(dec)
	, positive_position(pos_pos)
	, negative_position(neg_pos)
{
}

Limits::~Limits()
{
}

double Limits::get_velocity()
{
	return velocity;
}

double Limits::get_acceleration()
{
	return acceleration;
}

double Limits::get_deceleration()
{
	return deceleration;
}

double Limits::get_positive_position()
{
	return positive_position;
}

double Limits::get_negative_position()
{
	return negative_position;
}

void Limits::check_and_adjust_position(double& new_pos, const double& old_pos)
{
	if (new_pos > positive_position)
	{
		new_pos = positive_position;
	}
	else if (new_pos < negative_position)
	{
		new_pos = negative_position;
	}
	
	// add checks for velocity, acceleration and deceleration if the time context is clear
}
