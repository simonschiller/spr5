#pragma once
class Limits
{
public:
	Limits();
	Limits(double posPos, double negPos);
	Limits(double vel, double acc, double dec, double posPos, double negPos);
	~Limits();

	double get_velocity();
	double get_acceleration();
	double get_deceleration();
	double get_positive_position();
	double get_negative_position();

	void check_and_adjust_position(double& new_pos, const double& old_pos);


private:
	double velocity;
	double acceleration;
	double deceleration;
	double positive_position;
	double negative_position;
};

