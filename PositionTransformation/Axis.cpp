#include "Axis.h"

Axis::Axis(Transformation& transformation_, Limits& limits_)
: transform(transformation_)
, limits(limits_)
, act_position(0.0)
, old_position(0.0)
{

}

bool Axis::move(Coordinate& relative_coordinate)
{
	double distance = transform.transform_coordinates_to_axis_position(relative_coordinate);

	act_position = old_position + distance;

	limits.check_and_adjust_position(act_position, old_position);

	old_position = act_position;

	//TODO: call movement method of the drive (transfer position to the drive)
	return true;
}

double Axis::get_act_position()
{
	return act_position;
}
