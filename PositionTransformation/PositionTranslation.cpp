// PositionTranslation.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include "Axis.h"
#include "Coordinate.h"
#include "Limits.h"
#include "Point.h"
#include "Transformation.h"

int main()
{
	Transformation transformation_trigger_finger;
	Limits* limits_trigger_finger = new Limits();
	Axis* axis_trigger_finger = new Axis(transformation_trigger_finger, *limits_trigger_finger);
	Point* point_trigger_finger = new Point(*axis_trigger_finger);
	Coordinate* coordinate = new Coordinate();
	
	std::cout << "actual axis position before movement is " << axis_trigger_finger->get_act_position() << std::endl;
	std::cout << "expected was 0.0" << std::endl;
	coordinate->set_coordinate(100.0, 0.0, 0.0);
	point_trigger_finger->set_new_position(*coordinate);

	std::cout << "actual axis position after movement is " << axis_trigger_finger->get_act_position() << std::endl;
	std::cout << "expected was 25.5" << std::endl;

	coordinate->set_coordinate(50.0, 0.0, 0.0);
	point_trigger_finger->set_new_position(*coordinate);

	std::cout << "actual axis position after second movement is " << axis_trigger_finger->get_act_position() << std::endl;
	std::cout << "expected was 12.75" << std::endl;


	Transformation transformation_middle_finger(100, 500);
	Limits* limits_middle_finger = new Limits(500, 0);
	Axis* axis_middle_finger = new Axis(transformation_middle_finger, *limits_middle_finger);
	Point* point_middle_finger = new Point(*axis_middle_finger);
	Coordinate* coordinate_m = new Coordinate();
	
	std::cout << "" << std::endl;
	std::cout << "MIDDLE FINGER" << std::endl;
	std::cout << "actual axis position before movement is " << axis_middle_finger->get_act_position() << std::endl;
	std::cout << "expected was 0.0" << std::endl;
	coordinate_m->set_coordinate(100.0, 0.0, 0.0);
	point_middle_finger->set_new_position(*coordinate_m);

	std::cout << "actual axis position after movement is " << axis_middle_finger->get_act_position() << std::endl;
	std::cout << "expected was 500" << std::endl;

	coordinate_m->set_coordinate(50.0, 0.0, 0.0);
	point_middle_finger->set_new_position(*coordinate_m);

	std::cout << "actual axis position after second movement is " << axis_middle_finger->get_act_position() << std::endl;
	std::cout << "expected was 250" << std::endl;

	Transformation transformation_ring_finger(100, 500, -100, -500);
	Limits* limits_ring_finger = new Limits(500, -500);
	Axis* axis_ring_finger = new Axis(transformation_ring_finger, *limits_ring_finger);
	Point* point_ring_finger = new Point(*axis_ring_finger);
	Coordinate* coordinate_r = new Coordinate();

	std::cout << "" << std::endl;
	std::cout << "RING FINGER" << std::endl;
	std::cout << "actual axis position before movement is " << axis_ring_finger->get_act_position() << std::endl;
	std::cout << "expected was 0.0" << std::endl;
	coordinate_r->set_coordinate(100, 0.0, 0.0);
	point_ring_finger->set_new_position(*coordinate_r);

	std::cout << "actual axis position after movement is " << axis_ring_finger->get_act_position() << std::endl;
	std::cout << "expected was 500" << std::endl;

	coordinate_r->set_coordinate(-50.0, 0.0, 0.0);
	point_ring_finger->set_new_position(*coordinate_r);

	std::cout << "actual axis position after second movement is " << axis_ring_finger->get_act_position() << std::endl;
	std::cout << "expected was -250" << std::endl;

}