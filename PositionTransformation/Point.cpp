#include "Point.h"

Point::Point(Axis& axis_)
: axis(axis_)
, relative_point(nullptr)
{
	
}

Point::~Point() {}

void Point::set_new_position(Coordinate& new_pos)
{
	Coordinate relative_coordinate;
	old_pos = act_pos;
	act_pos = new_pos;

	double x = act_pos.get_x() - old_pos.get_x();
	double y = act_pos.get_y() - old_pos.get_y();
	double z = act_pos.get_z() - old_pos.get_z();

	relative_coordinate.set_coordinate(x, y, z);

	///\TODO: relativate movement with the movement of the relative Point. 

	axis.move(relative_coordinate);

}
