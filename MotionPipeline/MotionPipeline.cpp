// MotionPipeline.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
#include "../MotionPipeline.Core/MessageBus.h"
#include "../MotionPipeline.Posistage/PosistageSubscriber.h"
#include "../MotionPipeLine.UDPProducer/UDPProducer.h"
#include "../MotionPipeline.OpenFace/OpenFacePublisher.h"

int main()
{
	//initialize the message bus
	MessageBus message_bus;

	//register publishers / subscribers
	//LeapMotionPublisher leap_publisher(&message_bus);
	OpenFacePublisher openface_publisher(&message_bus);

	PosistageSubscriber posi_subscriber(&message_bus);

	UDPProducer  udp_publisher(5565, &message_bus);

	//start the message bus
	message_bus.run();
}