from scipy.spatial import distance as dist
from imutils.video import VideoStream
from imutils import face_utils
from threading import Thread
import numpy as np
import imutils
import dlib
import cv2
import socket

# Initialize dlib's face detector (HOG-based) and then create the facial landmark predictor
print("[INFO] Loading facial landmark predictor ...")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# Open an UDP socket for communication with the motion pipeline
print("[INFO] Opening connection to UDP socket ...")
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Start the video streaming thread
print("[INFO] Starting video stream thread ...")
vs = VideoStream(src=0).start()

# Loop over frames from the video
while True:

    # Grab the frame from the threaded video file stream, resize it and convert it to grayscale
    frame = vs.read()
    try:
        frame = imutils.resize(frame, width=640)
    except:
        vs = VideoStream(src=1).start() # Try other webcam if first one didn't work
        continue
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect faces in the grayscale frame
    rects = detector(gray, 0)

    # Loop over face detections
    for rect in rects:

        # Determine the facial landmarks for the face region, then convert the facial landmark (x, y)-coordinates to a numpy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # Extract the mouth coordinates
        mouth = shape[49:68]

        # Compute the convex hull of the mouth, then visualize the mouth
        hull = cv2.convexHull(mouth)
        cv2.drawContours(frame, [hull], -1, (0, 255, 0), 1)

        # Compute the hulls of the brow, then visualize them
        left_brow_hull = cv2.convexHull(shape[18:22])
        right_brow_hull = cv2.convexHull(shape[23:27])
        cv2.drawContours(frame, [left_brow_hull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [right_brow_hull], -1, (0, 255, 0), 1)

        # Compute the mouth open distance and print it to the screen
        upper_dist = (dist.euclidean(mouth[3], mouth[0]) + dist.euclidean(mouth[3], mouth[6])) / 2.0
        lower_dist = (dist.euclidean(mouth[0], mouth[9]) + dist.euclidean(mouth[6], mouth[9])) / 2.0
        cv2.putText(frame, "Mouth open upper distance: {:.2f}".format(upper_dist), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        cv2.putText(frame, "Mouth open lower distance: {:.2f}".format(lower_dist), (30, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

        # Compute the eyebrow coordinates and print them to the screen
        left_brow = shape.item(20, 1)
        right_brow = shape.item(25, 1)
        cv2.putText(frame, "Left brow: {:.2f}".format(left_brow), (30, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        cv2.putText(frame, "Right brow: {:.2f}".format(right_brow), (30, 120), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

        # Send the mouth open distance over the UDP socket
        message = f"label=mouth_open_upper_dist;x={upper_dist};y=0.0;z=0.0"
        sock.sendto(bytes(message, "utf-8"), ("127.0.0.1", 5565))
        message = f"label=mouth_open_lower_dist;x={upper_dist};y=0.0;z=0.0"
        sock.sendto(bytes(message, "utf-8"), ("127.0.0.1", 5565))

        # Send the eyebrow position over the UDP socket
        message = f"label=left_brow;x=0.0;y={left_brow};z=0.0"
        sock.sendto(bytes(message, "utf-8"), ("127.0.0.1", 5565))
        message = f"label=right_brow;x=0.0;y={right_brow};z=0.0"
        sock.sendto(bytes(message, "utf-8"), ("127.0.0.1", 5565))       

    # Show the frame
    cv2.imshow("Video capture", frame)
    key = cv2.waitKey(1) & 0xFF

    # If the Q key was pressed, break from the loop
    if key == ord("q"):
        break

# Do a bit of cleanup
cv2.destroyAllWindows()
vc.stop()
