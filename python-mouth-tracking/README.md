### Setup

Install Python 3.6 and `pip`, a newer version will not work: https://www.python.org/downloads/release/python-360/

Install all dependencies, please make sure to install them IN THIS ORDER:
* `pip install numpy`
* `pip install scipy`
* `pip install imutils`
* `pip install opencv-python`
* `pip install cmake`
* `pip install dlib`

Download the `shape_predictor_68_face_landmarks.dat` file from the following link/repository. Place it in the `python-mouth-tracking` folder and DO NOT RENAME it.
https://github.com/mauckc/mouth-open/blob/master/shape_predictor_68_face_landmarks.dat

You can also run the `install-dependencies.ps1` script, which takes care of installing dependencies and downloading the model.

### Run tracking

`python tracking.py`
