# Install dependencies
pip install numpy
pip install scipy
pip install imutils
pip install opencv-python
pip install cmake
pip install dlib

# Download and place model
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest https://github.com/mauckc/mouth-open/raw/master/shape_predictor_68_face_landmarks.dat -OutFile shape_predictor_68_face_landmarks.dat
