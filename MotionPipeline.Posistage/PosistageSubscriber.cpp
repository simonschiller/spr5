#include "pch.h"
#include "PosistageSubscriber.h"
#include <chrono>
#include <cstdint>
#include <iostream>

PosistageSubscriber::PosistageSubscriber(MessageBus* message_bus) : Subscriber(message_bus)
{
	m_instance = new PosiStageInstance();
}

PosistageSubscriber::~PosistageSubscriber()
{
	delete m_instance;
}

void PosistageSubscriber::on_notify(Message* message)
{
	CoordinateMessage* coordinate_message = static_cast<CoordinateMessage*>(message);

	if (coordinate_message != nullptr)
	{
		std::cout << *coordinate_message << std::endl;
		string label = coordinate_message->get_label();
		TransformationComposition compX;
		TransformationComposition compY;
		TransformationComposition compZ;
		if (m_data.find(label) == m_data.end())
		{
			int id = m_data.size();
			m_instance->addTracker(id, label);
			compX.limits = new Limits();
			compX.axis = new Axis(compX.transformation, *compX.limits);
			compX.point = new Point(*compX.axis);
			compX.coordinate = new Coordinate();
			compX.id = id;

			compY.limits = new Limits();
			compY.axis = new Axis(compY.transformation, *compY.limits);
			compY.point = new Point(*compY.axis);
			compY.coordinate = new Coordinate();
			compY.id = id;

			compZ.limits = new Limits();
			compZ.axis = new Axis(compZ.transformation, *compZ.limits);
			compZ.point = new Point(*compZ.axis);
			compZ.coordinate = new Coordinate();
			compZ.id = id;
		}
		else
		{
			compX = m_data[label][PosiStageInstance::TrackerPosition::PositionX];
			compY = m_data[label][PosiStageInstance::TrackerPosition::PositionY];
			compZ = m_data[label][PosiStageInstance::TrackerPosition::PositionZ];
		}

		compX.coordinate->set_coordinate(coordinate_message->get_x(), coordinate_message->get_y(), coordinate_message->get_z());
		compY.coordinate->set_coordinate(coordinate_message->get_y(), 0, 0);
		compZ.coordinate->set_coordinate(coordinate_message->get_z(), 0, 0);
		compX.point->set_new_position(*compX.coordinate);
		compY.point->set_new_position(*compY.coordinate);
		compZ.point->set_new_position(*compZ.coordinate);

		m_data[label][PosiStageInstance::TrackerPosition::PositionX] = compX;
		m_data[label][PosiStageInstance::TrackerPosition::PositionY] = compY;
		m_data[label][PosiStageInstance::TrackerPosition::PositionZ] = compZ;

		m_instance->setValue(label, compX.axis->get_act_position(), PosiStageInstance::TrackerPosition::PositionX);
		m_instance->setValue(label, compY.axis->get_act_position(), PosiStageInstance::TrackerPosition::PositionY);
		m_instance->setValue(label, compZ.axis->get_act_position(), PosiStageInstance::TrackerPosition::PositionZ);

		auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		m_instance->update(timestamp, true, timestamp - m_timestamp >= 1000);
		m_timestamp = timestamp;
	}
}