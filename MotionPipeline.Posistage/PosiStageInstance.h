
#include "udp_socket.h"
#include "PosiStageData.h"
using namespace std;
class PosiStageInstance
{
public:
	PosiStageInstance(const string& encoderName = "PSN_SERVER",
		const string& address = PSN_DEFAULT_UDP_MULTICAST_ADDR,
		int port = PSN_DEFAULT_UDP_PORT);
	~PosiStageInstance();

	enum class TrackerPosition
	{
		PositionX,
		PositionY,
		PositionZ,
		SpeedX,
		SpeedY,
		SpeedZ,
		OrientationX,
		OrientationY,
		OrientationZ,
	};

	psn::psn_tracker& addTracker(int id, const string& name);
	void removeTracker(const string& name);
	void setValue(const string& tracker, double value, const TrackerPosition& posType);
	void update(uint64_t timestamp, bool sendData, bool sendInfo);

	string getAddress() const { return m_address; }
	int getPort() const { return m_port; }

private:
	udp_socket& getSocket() { return *m_socket_server; }
	psn::psn_encoder& getEncoder() { return *m_encoder; }
	int getTrackerIdForName(const string& name) const;

	wsa_session m_session;
	udp_socket* m_socket_server;
	psn::psn_tracker_array m_trackers;
	psn::psn_encoder* m_encoder;

	int generateID();
	bool trackerExists(const string& name) const;
	bool trackerExists(int id) const;
	string m_address = PSN_DEFAULT_UDP_MULTICAST_ADDR;
	int m_port = PSN_DEFAULT_UDP_PORT;
};



