#pragma once

#include "..//MotionPipeline.Core/Subscriber.h"
#include "../MotionPipeline.Core/MessageBus.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include "../MotionPipeline.Domain/Axis.h"
#include "../MotionPipeline.Domain/Limits.h"
#include "../MotionPipeline.Domain/Coordinate.h"
#include "../MotionPipeline.Domain/Transformation.h"
#include "../MotionPipeline.Domain/Point.h"
#include "PosiStageInstance.h"
#include <map>

using namespace std;
class PosistageSubscriber : public Subscriber
{
public:
	PosistageSubscriber(MessageBus* messageBus);
	~PosistageSubscriber();

	virtual void on_notify(Message* message) override;
private:

	struct TransformationComposition
	{
		int id = -1;
		Transformation transformation;
		Limits* limits = nullptr;
		Axis* axis = nullptr;
		Point* point = nullptr;
		Coordinate* coordinate = nullptr;
	};

	uint64_t m_timestamp = 0;
	PosiStageInstance* m_instance;
	map<string, map<PosiStageInstance::TrackerPosition, TransformationComposition>> m_data;


};

