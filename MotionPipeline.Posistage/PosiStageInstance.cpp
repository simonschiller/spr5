#include "pch.h"
#include "PosiStageInstance.h"
#include <iostream>
PosiStageInstance::PosiStageInstance(const std::string& encoderName, const std::string& address, int port)
{
	m_session = wsa_session();
	m_socket_server = new udp_socket();
	m_socket_server->enable_send_message_multicast();
	m_encoder = new ::psn::psn_encoder(encoderName);
	m_address = address;
	m_port = port;

}

PosiStageInstance::~PosiStageInstance()
{
	delete m_socket_server;
	delete m_encoder;
}

int PosiStageInstance::getTrackerIdForName(const std::string& name) const
{
	for (auto& tracker : m_trackers)
	{
		if (tracker.second.name_ == name)
			return tracker.second.id_;
	}

	return -1;
}

int PosiStageInstance::generateID()
{
	int id = 1;
	bool idFound = true;
	while (idFound)
	{
		idFound = false;
		for (int i = 0; i < m_trackers.size(); ++i)
		{
			if (m_trackers[i].id_ == id)
			{
				id++;
				break;
			}
		}
	}
	return 0;
}

bool PosiStageInstance::trackerExists(const std::string& name) const
{
	for (auto& tracker : m_trackers)
	{
		if (tracker.second.name_ == name)
			return true;
	}

	return false;
}

bool PosiStageInstance::trackerExists(int id) const
{

	for (auto& tracker : m_trackers)
	{
		if (tracker.second.id_ == id)
			return true;
	}

	return false;
}


psn::psn_tracker& PosiStageInstance::addTracker(int id, const std::string& name)
{
	if (trackerExists(id) || trackerExists(name))
		throw "Id or name already exists!";

	m_trackers[id] = ::psn::psn_tracker(id, name);
	return m_trackers[id];
}

void PosiStageInstance::removeTracker(const std::string& name)
{
	for (auto it = m_trackers.begin(); it != m_trackers.end(); ++it)
	{
		if (it->second.name_ == name)
			m_trackers.erase(it);
	}
}

void PosiStageInstance::setValue(const std::string& tracker, double value, const TrackerPosition& posType)
{
	int idx = getTrackerIdForName(tracker);
	switch (posType)
	{
	case TrackerPosition::PositionX:
		m_trackers[idx].pos_.x = value;
		break;
	case TrackerPosition::PositionY:
		m_trackers[idx].pos_.y = value;
		break;
	case TrackerPosition::PositionZ:
		m_trackers[idx].pos_.z = value;
		break;
	case TrackerPosition::SpeedX:
		m_trackers[idx].speed_.x = value;
		break;
	case TrackerPosition::SpeedY:
		m_trackers[idx].speed_.y = value;
		break;
	case TrackerPosition::SpeedZ:
		m_trackers[idx].speed_.z = value;
		break;
	case TrackerPosition::OrientationX:
		m_trackers[idx].ori_.x = value;
		break;
	case TrackerPosition::OrientationY:
		m_trackers[idx].ori_.y = value;
		break;
	case TrackerPosition::OrientationZ:
		m_trackers[idx].ori_.z = value;
		break;
	default:
		break;
	}
}

void PosiStageInstance::update(uint64_t timestamp, bool sendData, bool sendInfo)
{

	getEncoder().set_trackers(m_trackers);

	if (sendData)
	{
		list< string > data_packets;
		if (getEncoder().encode_data(data_packets, timestamp))
		{
			cout << "Sending PSN_DATA_PACKET : "
				<< "Frame Id = " << (int)getEncoder().get_last_encode_data_packet_frame_id()
				<< " , Packet Count = " << data_packets.size() << endl;
			list< string >::iterator it;

			for (auto item : data_packets)
			{
				getSocket().send_message(m_address, m_port, item);
			}
		}
	}


	if (sendInfo)
	{
		list< string > info_packets;
		if (getEncoder().encode_info(info_packets, timestamp))
		{
			cout << "Sending PSN_INFO_PACKET : "
				<< "Frame Id = " << (int)getEncoder().get_last_encode_info_packet_frame_id()
				<< " , Packet Count = " << info_packets.size() << endl;

			for (auto item : info_packets)
			{
				getSocket().send_message(m_address, m_port, item);
			}
		}
	}
}
