#pragma once
#include <stdint.h>
#include <map>
#include <vector>
#include <string>
#include <list>

namespace psn
{

// PSN VERSION
#define PSN_HIGH_VERSION                2
#define PSN_LOW_VERSION                 0

// DEFAULT UDP PORT and MULTICAST ADDRESS
#define PSN_DEFAULT_UDP_PORT            56565
#define PSN_DEFAULT_UDP_MULTICAST_ADDR  "236.10.10.10"

// MAX UDP PACKET SIZE
#define PSN_MAX_UDP_PACKET_SIZE         1500

// - PSN INFO -
#define PSN_INFO_PACKET                 0x6756
#define PSN_INFO_PACKET_HEADER          0x0000
#define PSN_INFO_SYSTEM_NAME            0x0001
#define PSN_INFO_TRACKER_LIST           0x0002
//#define PSN_INFO_TRACKER              tracker_id
#define PSN_INFO_TRACKER_NAME           0x0000

// - PSN DATA -
#define PSN_DATA_PACKET                 0x6755
#define PSN_DATA_PACKET_HEADER          0x0000
#define PSN_DATA_TRACKER_LIST           0x0001
//#define PSN_DATA_TRACKER              tracker_id
#define PSN_DATA_TRACKER_POS            0x0000
#define PSN_DATA_TRACKER_SPEED          0x0001
#define PSN_DATA_TRACKER_ORI            0x0002
#define PSN_DATA_TRACKER_STATUS         0x0003

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	struct float3
	{
		float3(float px = 0, float py = 0, float pz = 0)
			: x(px), y(py), z(pz)
		{}

		float x, y, z;
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	struct psn_tracker
	{
		psn_tracker(uint16_t id = 0,
			const ::std::string & name = "",
			float3 pos = float3(),
			float3 speed = float3(),
			float3 ori = float3(),
			float validity = 0.f)
			: id_(id)
			, name_(name)
			, pos_(pos)
			, speed_(speed)
			, ori_(ori)
			, validity_(validity)
		{}

		uint16_t id_;

		::std::string name_;

		float3 pos_;
		float3 speed_;
		float3 ori_;

		float validity_;
	};

	typedef ::std::map< uint16_t, psn_tracker > psn_tracker_array; // map< id , psn_tracker >

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// psn_chunk_header
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	struct psn_chunk_header
	{
		union
		{
			struct
			{
				uint32_t    id_ : 16;   // chunk ID
				uint32_t    data_len_ : 15;   // length of chunk data or sub chunks
				uint32_t    has_subchunks_ : 1;    // does it contain other chunks
			};

			uint32_t bits_;                        // to set all bits at once
		};
	};

	static size_t SIZE_OF_PSN_CHUNK_HEADER = sizeof(psn_chunk_header);



//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// psn_chunk_data
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_chunk_data
	{
	public:

		psn_chunk_data(void) {}
		virtual ~psn_chunk_data(void) {}

		virtual size_t get_data_size(void) const = 0;
		virtual void write_buffer(void* buffer) const = 0;
		virtual void read_buffer(void* buffer, size_t buffer_size) = 0;
	};
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// psn_chunk
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	class psn_chunk
	{
	public:

		psn_chunk(uint16_t id, psn_chunk_data* data = 0, const ::std::string & name = "") : parent_(0)
			, id_(id)
			, name_(name)
			, data_(data)
		{
			header_.bits_ = 0;
		};
		virtual ~psn_chunk(void) 
		{
			// delete data
			if (data_ != 0)
			{
				delete data_;
				data_ = 0;
			}

			// Remove from parent
			if (parent_ != 0)
			{
				parent_->remove_child(this, false);
			}

			// delete children
			remove_all_children(true);
		};

		uint16_t get_id(void) const { return id_; };

		void set_name(const ::std::string& name) { name_ = name; };
		::std::string get_name(void) const {return name_;}
		size_t get_chunk_size(void) const {
			size_t chunk_size = 0;

			chunk_size += SIZE_OF_PSN_CHUNK_HEADER;

			if (data_ != 0)
				chunk_size += data_->get_data_size();

			for (size_t i = 0; i < children_.size(); ++i)
			{
				chunk_size += children_[i]->get_chunk_size();
			}

			return chunk_size;
		};
		psn_chunk_data* get_data(void) const 
		{
			return data_;
		};

		bool encode(void* buffer, size_t buffer_size)
		{
			size_t data_size = get_chunk_size();

			if (buffer_size < data_size)
				return false;

			// encode header
			header_.id_ = id_;
			header_.data_len_ = data_size - SIZE_OF_PSN_CHUNK_HEADER;
			header_.has_subchunks_ = (children_.empty() ? 0 : 1);

			memcpy(buffer, &header_, SIZE_OF_PSN_CHUNK_HEADER);

			// seek through buffer 
			buffer = (char*)buffer + SIZE_OF_PSN_CHUNK_HEADER;
			buffer_size -= SIZE_OF_PSN_CHUNK_HEADER;

			// encode data only if we don't have children
			if (data_ != 0 && children_.empty())
			{
				data_size = data_->get_data_size();

				if (buffer_size < data_size)
					return false;

				if (data_size > 0)
				{
					data_->write_buffer(buffer);

					// seek through buffer 
					buffer = (char*)buffer + data_size;
					buffer_size -= data_size;
				}
			}

			// encode children
			for (size_t i = 0; i < children_.size(); ++i)
			{
				data_size = children_[i]->get_chunk_size();

				if (buffer_size < data_size)
					return false;

				if (!children_[i]->encode(buffer, buffer_size))
					return false;

				// seek through buffer 
				buffer = (char*)buffer + data_size;
				buffer_size -= data_size;
			}

			return true;
		};
		bool decode(void* buffer, size_t buffer_size)
		{
			if (buffer_size < SIZE_OF_PSN_CHUNK_HEADER)
				return false;

			// decode header
			memcpy(&header_, buffer, SIZE_OF_PSN_CHUNK_HEADER);

			if (header_.id_ != id_ || buffer_size < header_.data_len_)
				return false;

			// seek through buffer 
			buffer = (char*)buffer + SIZE_OF_PSN_CHUNK_HEADER;
			buffer_size -= SIZE_OF_PSN_CHUNK_HEADER;

			// decode children or data
			bool result = true;

			if (header_.has_subchunks_)
			{
				while (buffer_size >= SIZE_OF_PSN_CHUNK_HEADER && result)
				{
					psn_chunk_header child_header;

					// decode child header
					memcpy(&child_header, buffer, SIZE_OF_PSN_CHUNK_HEADER);

					// decode child
					result = decode_child(child_header.id_, buffer, buffer_size);

					// seek through buffer 
					if (result)
					{
						buffer = (char*)buffer + SIZE_OF_PSN_CHUNK_HEADER + child_header.data_len_;
						buffer_size -= SIZE_OF_PSN_CHUNK_HEADER + child_header.data_len_;
					}
				}
			}
			else
			{
				// Decode chunk data
				if (data_ != 0)
					data_->read_buffer(buffer, header_.data_len_);
			}

			return result;
		};

	protected:

		virtual bool decode_child(uint16_t child_id, void* buffer, size_t buffer_size)
		{
			psn_chunk* child = get_child_by_id(child_id);

			if (child != 0)
			{
				return child->decode(buffer, buffer_size);
			}

			return true;
		};

	public:

		psn_chunk* get_parent(void) const
		{
			return parent_;
		};

		void add_child(psn_chunk* child)
		{
			if (child == 0)
				return;

			// If it's already a child, do not add it again
			// Also check for infinite loop
			if (!find_child(child) && !this->find_parent(child))
			{
				if (child->parent_ != 0)
					child->parent_->remove_child(child, false);

				child->parent_ = this;
				children_.push_back(child);
			}
		};
		void insert_child(int index, psn_chunk* child)
		{
			if (child == 0)
				return;

			// If it's already a child, do not add it again
			// Also check for infinite loop
			if (!find_child(child) && !this->find_parent(child))
			{
				if (child->parent_ != 0)
					child->parent_->remove_child(child, false);

				child->parent_ = this;

				if (index >= 0 && index < (int)children_.size())
					children_.insert(children_.begin() + index, child);
				else
					children_.push_back(child);
			}
		};

		bool remove_child_by_index(int index, bool auto_delete = true)
		{
			if (index >= 0 && index < (int)children_.size())
			{
				if (auto_delete)
					delete children_[index];
				else
				{
					children_[index]->parent_ = 0;
					children_.erase(children_.begin() + index);
				}
				return true;
			}

			return false;
		};
		bool remove_child_by_id(uint16_t id, bool auto_delete = true)
		{
			for (size_t i = 0; i < children_.size(); ++i)
			{
				if (children_[i]->get_id() == id)
				{
					if (auto_delete)
						delete children_[i];
					else
					{
						children_[i]->parent_ = 0;
						children_.erase(children_.begin() + i);
					}
					return true;
				}
			}

			return false;
		};
		bool remove_child(psn_chunk* child, bool auto_delete = true)
		{
			for (size_t i = 0; i < children_.size(); ++i)
			{
				if (children_[i] == child)
				{
					if (auto_delete)
						delete children_[i];
					else
					{
						children_[i]->parent_ = 0;
						children_.erase(children_.begin() + i);
					}
					return true;
				}
			}

			return false;
		};

		void remove_all_children(bool auto_delete = true)
		{
			if (auto_delete)
			{
				while (!children_.empty())
				{
					delete children_[0];
				}
			}

			for (size_t i = 0; i < children_.size(); ++i)
			{
				children_[i]->parent_ = 0;
			}

			children_.clear();
		};

		psn_chunk* get_child_by_id(uint16_t id)
		{
			for (size_t i = 0; i < children_.size(); ++i)
			{
				if (children_[i]->get_id() == id)
				{
					return children_[i];
				}
			}

			return 0;
		};
		::std::vector< psn_chunk* > get_children(void)
		{
			return children_;
		};

		bool find_child(psn_chunk* child)
		{
			if (child == 0)
				return false;

			for (size_t i = 0; i < children_.size(); ++i)
			{
				if (child == children_[i])
					return true;

				if (children_[i]->find_child(child))
					return true;
			}

			return false;
		};
		bool find_parent(psn_chunk* parent)
		{
			if (parent == 0)
				return false;

			psn_chunk* next_parent = get_parent();

			while (next_parent != 0)
			{
				if (next_parent == parent)
					return true;

				next_parent = next_parent->get_parent();
			}

			return false;
		};

	private:

		psn_chunk* parent_;
		::std::vector< psn_chunk* > children_;

	private:
		uint16_t                     id_;
		::std::string                name_;

		psn_chunk_header             header_;
		psn_chunk_data* data_;
	};




	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// psn_packet_header_data
	//
	// It is used by :
	// ----------------------
	// PSN_INFO_PACKET_HEADER 
	// PSN_DATA_PACKET_HEADER
	//-----------------------
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_packet_header_data : public psn_chunk_data
	{
	public:

		psn_packet_header_data(void)
			: timestamp_usec_(0)
			, version_high_(0)
			, version_low_(0)
			, frame_id_(0)
			, frame_packet_count_(0)
		{}
		virtual ~psn_packet_header_data(void) {}

		virtual size_t get_data_size(void) const
		{
			static size_t size_of_data = sizeof(timestamp_usec_) +
				sizeof(version_high_) +
				sizeof(version_low_) +
				sizeof(frame_id_) +
				sizeof(frame_packet_count_);

			return size_of_data;
		}

		virtual void write_buffer(void* buffer) const
		{
			memcpy(buffer, &timestamp_usec_, sizeof(timestamp_usec_));

			buffer = (char*)buffer + sizeof(timestamp_usec_);
			memcpy(buffer, &version_high_, sizeof(version_high_));

			buffer = (char*)buffer + sizeof(version_high_);
			memcpy(buffer, &version_low_, sizeof(version_low_));

			buffer = (char*)buffer + sizeof(version_low_);
			memcpy(buffer, &frame_id_, sizeof(frame_id_));

			buffer = (char*)buffer + sizeof(frame_id_);
			memcpy(buffer, &frame_packet_count_, sizeof(frame_packet_count_));
		}

		virtual void read_buffer(void* buffer, size_t buffer_size)
		{
			if (buffer_size < get_data_size())
				return;

			memcpy(&timestamp_usec_, buffer, sizeof(timestamp_usec_));

			buffer = (char*)buffer + sizeof(timestamp_usec_);
			memcpy(&version_high_, buffer, sizeof(version_high_));

			buffer = (char*)buffer + sizeof(version_high_);
			memcpy(&version_low_, buffer, sizeof(version_low_));

			buffer = (char*)buffer + sizeof(version_low_);
			memcpy(&frame_id_, buffer, sizeof(frame_id_));

			buffer = (char*)buffer + sizeof(frame_id_);
			memcpy(&frame_packet_count_, buffer, sizeof(frame_packet_count_));
		}

	public:

		uint64_t timestamp_usec_;
		uint8_t  version_high_;
		uint8_t  version_low_;
		uint8_t  frame_id_;
		uint8_t  frame_packet_count_;
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// psn_vec3_data
	//
	// It is used by :
	// ----------------------
	// PSN_DATA_TRACKER_POS 
	// PSN_DATA_TRACKER_SPEED
	// PSN_DATA_TRACKER_ORI
	//-----------------------
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_vec3_data : public psn_chunk_data
	{
	public:

		psn_vec3_data(float x = 0, float y = 0, float z = 0)
			: vec_(x, y, z)
		{}

		virtual ~psn_vec3_data(void) {}

		virtual size_t get_data_size(void) const
		{
			static size_t size_of_data = sizeof(vec_);

			return size_of_data;
		}

		virtual void write_buffer(void* buffer) const
		{
			memcpy(buffer, &vec_, sizeof(vec_));
		}

		virtual void read_buffer(void* buffer, size_t buffer_size)
		{
			if (buffer_size < get_data_size())
				return;

			memcpy(&vec_, buffer, sizeof(vec_));
		}

	public:

		float3    vec_;
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// psn_vec3_data
	//
	// It is used by :
	// ----------------------
	// PSN_DATA_TRACKER_STATUS
	//-----------------------
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_tracker_status_data : public psn_chunk_data
	{
	public:

		psn_tracker_status_data(float validity = 0.f)
			: validity_(validity)
		{}

		virtual ~psn_tracker_status_data(void) {}

		virtual size_t get_data_size(void) const
		{
			static size_t size_of_data = sizeof(validity_);

			return size_of_data;
		}

		virtual void write_buffer(void* buffer) const
		{
			memcpy(buffer, &validity_, sizeof(validity_));
		}

		virtual void read_buffer(void* buffer, size_t buffer_size)
		{
			if (buffer_size < get_data_size())
				return;

			memcpy(&validity_, buffer, sizeof(validity_));
		}

	public:

		float    validity_;
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// psn_name_data
	//
	// It is used by :
	// ----------------------
	// PSN_INFO_SYSTEM_NAME
	// PSN_INFO_TRACKER_NAME
	//-----------------------
	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_name_data : public psn_chunk_data
	{
	public:

		psn_name_data(const ::std::string& name = "")
			: name_(name)
		{}

		virtual ~psn_name_data(void) {}

		virtual size_t get_data_size(void) const
		{
			return name_.length();
		}

		virtual void write_buffer(void* buffer) const
		{
			memcpy(buffer, name_.c_str(), name_.length());
		}

		virtual void read_buffer(void* buffer, size_t buffer_size)
		{
			name_.clear();

			if (buffer_size > 0)
				name_ = ::std::string((char*)buffer, buffer_size);
		}

	public:

		::std::string name_;
	};

	class psn_encoder
	{
	public:
		psn_encoder(const ::std::string& system_name = "")
			: info_packet_frame_id_(0)
			, data_packet_frame_id_(0)
		{
			// PSN_DATA 
			psn_data_packet_ = new psn_chunk(PSN_DATA_PACKET, 0, "PSN_DATA_PACKET");
			psn_data_packet_header_ = new psn_chunk(PSN_DATA_PACKET_HEADER, new ::psn::psn_packet_header_data(), "PSN_DATA_PACKET_HEADER");
			psn_data_tracker_list_ = new psn_chunk(PSN_DATA_TRACKER_LIST, 0, "PSN_DATA_TRACKER_LIST");
			// PSN_DATA_TRACKER chunks will be dynamically instantiated on encode_data 

			psn_data_packet_->add_child(psn_data_packet_header_);
			psn_data_packet_->add_child(psn_data_tracker_list_);

			// PSN_INFO
			psn_info_packet_ = new psn_chunk(PSN_INFO_PACKET, 0, "PSN_INFO_PACKET");
			psn_info_packet_header_ = new psn_chunk(PSN_INFO_PACKET_HEADER, new ::psn::psn_packet_header_data(), "PSN_INFO_PACKET_HEADER");
			psn_info_system_name_ = new psn_chunk(PSN_INFO_SYSTEM_NAME, new ::psn::psn_name_data(system_name), "PSN_INFO_SYSTEM_NAME");
			psn_info_tracker_list_ = new psn_chunk(PSN_INFO_TRACKER_LIST, 0, "PSN_INFO_TRACKER_LIST");
			// PSN_INFO_TRACKER chunks will be dynamically instantiated on encode_info

			psn_info_packet_->add_child(psn_info_packet_header_);
			psn_info_packet_->add_child(psn_info_system_name_);
			psn_info_packet_->add_child(psn_info_tracker_list_);
		};
		~psn_encoder(void)
		{
			delete psn_data_packet_;
			psn_data_packet_ = 0;

			delete psn_info_packet_;
			psn_info_packet_ = 0;
		};

		void set_trackers(const ::psn::psn_tracker_array& trackers)
		{
			trackers_ = trackers;
		};

		uint8_t get_last_encode_info_packet_frame_id(void) const { return info_packet_frame_id_; }
		uint8_t get_last_encode_data_packet_frame_id(void) const { return data_packet_frame_id_; }

		bool encode_info(::std::list< ::std::string >& encoded_info_packets, uint64_t timestamp = 0)
		{
			// Update PSN_INFO_TRACKER_LIST chunk and split all the PSN_INFO_TRACKER chunks into multiple packets
			::std::vector< ::std::vector< psn_chunk* > > psn_info_tracker_packets;

			{
				psn_info_tracker_list_->remove_all_children();

				size_t static_chunks_size = psn_info_packet_->get_chunk_size();
				size_t buffer_size = static_chunks_size;

				::std::vector< psn_chunk* > psn_info_tracker_packet;

				::psn::psn_tracker_array::iterator it;
				for (it = trackers_.begin(); it != trackers_.end(); ++it)
				{
					psn_chunk* psn_info_tracker = new psn_chunk(it->second.id_, 0, "PSN_INFO_TRACKER");

					psn_info_tracker->add_child(new psn_chunk(PSN_INFO_TRACKER_NAME, new psn_name_data(it->second.name_), "PSN_INFO_TRACKER_NAME"));

					if (!psn_info_tracker_packet.empty() && // be sure to push at least one tracker per packet
						buffer_size + psn_info_tracker->get_chunk_size() > PSN_MAX_UDP_PACKET_SIZE)
					{
						psn_info_tracker_packets.push_back(psn_info_tracker_packet);
						psn_info_tracker_packet.clear();

						buffer_size = static_chunks_size;
					}

					buffer_size += psn_info_tracker->get_chunk_size();
					psn_info_tracker_packet.push_back(psn_info_tracker);
				}

				if (!psn_info_tracker_packet.empty())
					psn_info_tracker_packets.push_back(psn_info_tracker_packet);
			}

			// Update PSN_INFO_PACKET_HEADER chunk
			{
				psn_packet_header_data* data = (psn_packet_header_data*)psn_info_packet_header_->get_data();

				if (data != 0)
				{
					data->timestamp_usec_ = timestamp;
					data->version_high_ = PSN_HIGH_VERSION;
					data->version_low_ = PSN_LOW_VERSION;
					data->frame_id_ = ++info_packet_frame_id_;
					data->frame_packet_count_ = (uint8_t)psn_info_tracker_packets.size();
				}
			}

			// Encode all packets
			for (size_t i = 0; i < psn_info_tracker_packets.size(); ++i)
			{
				psn_info_tracker_list_->remove_all_children();

				::std::vector< psn_chunk* >& psn_info_tracker_packet = psn_info_tracker_packets[i];

				for (size_t j = 0; j < psn_info_tracker_packet.size(); ++j)
				{
					psn_info_tracker_list_->add_child(psn_info_tracker_packet[j]);
				}

				size_t buffer_size = psn_info_packet_->get_chunk_size();

				char* buffer = (char*)malloc(buffer_size);

				if (psn_info_packet_->encode(buffer, buffer_size))
				{
					encoded_info_packets.push_back(::std::string(buffer, buffer_size));
				}

				free(buffer);
			}

			return true;
		};
		bool encode_data(::std::list< ::std::string >& encoded_data_packets, uint64_t timestamp = 0)
		{
			// Update PSN_TRACKER_SECTION chunk and split all the PSN_DATA_TRACKER chunks into multiple packets
			::std::vector< ::std::vector< psn_chunk* > > psn_data_tracker_packets;

			{
				psn_data_tracker_list_->remove_all_children();

				size_t static_chunks_size = psn_data_packet_->get_chunk_size();
				size_t buffer_size = static_chunks_size;

				::std::vector< psn_chunk* > psn_data_tracker_packet;

				::psn::psn_tracker_array::iterator it;
				for (it = trackers_.begin(); it != trackers_.end(); ++it)
				{
					psn_chunk* psn_data_tracker = new psn_chunk(it->second.id_, 0, "PSN_DATA_TRACKER");

					psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_POS, new psn_vec3_data(it->second.pos_.x, it->second.pos_.y, it->second.pos_.z), "PSN_DATA_TRACKER_POS"));
					psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_SPEED, new psn_vec3_data(it->second.speed_.x, it->second.speed_.y, it->second.speed_.z), "PSN_DATA_TRACKER_SPEED"));
					psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_ORI, new psn_vec3_data(it->second.ori_.x, it->second.ori_.y, it->second.ori_.z), "PSN_DATA_TRACKER_ORI"));
					psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_STATUS, new psn_tracker_status_data(it->second.validity_), "PSN_DATA_TRACKER_STATUS"));

					if (!psn_data_tracker_packet.empty() && // be sure to push at least one tracker per packet
						buffer_size + psn_data_tracker->get_chunk_size() > PSN_MAX_UDP_PACKET_SIZE)
					{
						psn_data_tracker_packets.push_back(psn_data_tracker_packet);
						psn_data_tracker_packet.clear();

						buffer_size = static_chunks_size;
					}

					buffer_size += psn_data_tracker->get_chunk_size();
					psn_data_tracker_packet.push_back(psn_data_tracker);
				}

				if (!psn_data_tracker_packet.empty())
					psn_data_tracker_packets.push_back(psn_data_tracker_packet);
			}

			// Update PSN_DATA_PACKET_HEADER chunk
			{
				psn_packet_header_data* data = (psn_packet_header_data*)psn_data_packet_header_->get_data();

				if (data != 0)
				{
					data->timestamp_usec_ = timestamp;
					data->version_high_ = PSN_HIGH_VERSION;
					data->version_low_ = PSN_LOW_VERSION;
					data->frame_id_ = ++data_packet_frame_id_;
					data->frame_packet_count_ = (uint8_t)psn_data_tracker_packets.size();
				}
			}

			// Encode all packets
			for (size_t i = 0; i < psn_data_tracker_packets.size(); ++i)
			{
				psn_data_tracker_list_->remove_all_children();

				::std::vector< psn_chunk* >& psn_data_tracker_packet = psn_data_tracker_packets[i];

				for (int j = 0; j < (int)psn_data_tracker_packet.size(); ++j)
				{
					psn_data_tracker_list_->add_child(psn_data_tracker_packet[j]);
				}

				size_t buffer_size = psn_data_packet_->get_chunk_size();

				char* buffer = (char*)malloc(buffer_size);

				if (psn_data_packet_->encode(buffer, buffer_size))
				{
					encoded_data_packets.push_back(::std::string(buffer, buffer_size));
				}

				free(buffer);
			}

			return true;
		};

	private:

		psn_chunk* psn_data_packet_;
		psn_chunk* psn_data_packet_header_;
		psn_chunk* psn_data_tracker_list_;

		psn_chunk* psn_info_packet_;
		psn_chunk* psn_info_packet_header_;
		psn_chunk* psn_info_system_name_;
		psn_chunk* psn_info_tracker_list_;

	private:

		::psn::psn_tracker_array    trackers_;

		uint8_t                        info_packet_frame_id_;
		uint8_t                        data_packet_frame_id_;
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_data_tracker_list_chunk : public psn_chunk
	{
	public:

		psn_data_tracker_list_chunk(uint16_t id, psn_chunk_data* data = 0, const ::std::string & name = "")
			: psn_chunk(id, data, name)
		{}

		virtual ~psn_data_tracker_list_chunk(void)
		{}

	protected:

		// re-implement decode_child for PSN_DATA_TRACKER_LIST chunk
		// on decode_child, it will automatically add its own child
		virtual bool decode_child(uint16_t child_id, void* buffer, size_t buffer_size)
		{
			if (get_child_by_id(child_id) == 0)
			{
				psn_chunk* psn_data_tracker = new psn_chunk(child_id, 0, "PSN_DATA_TRACKER");

				psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_POS, new psn_vec3_data(), "PSN_DATA_TRACKER_POS"));
				psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_SPEED, new psn_vec3_data(), "PSN_DATA_TRACKER_SPEED"));
				psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_ORI, new psn_vec3_data(), "PSN_DATA_TRACKER_ORI"));
				psn_data_tracker->add_child(new psn_chunk(PSN_DATA_TRACKER_STATUS, new psn_tracker_status_data(), "PSN_DATA_TRACKER_STATUS"));

				add_child(psn_data_tracker);
			}

			return psn_chunk::decode_child(child_id, buffer, buffer_size);
		}
	};

	//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	class psn_info_tracker_list_chunk : public psn_chunk
	{
	public:

		psn_info_tracker_list_chunk(uint16_t id, psn_chunk_data* data = 0, const ::std::string & name = "")
			: psn_chunk(id, data, name)
		{}

		virtual ~psn_info_tracker_list_chunk(void)
		{}

	protected:

		// re-implement decode_child for PSN_INFO_TRACKER_LIST chunk
		// on decode_child, it will automatically add its own child
		virtual bool decode_child(uint16_t child_id, void* buffer, size_t buffer_size)
		{
			if (get_child_by_id(child_id) == 0)
			{
				psn_chunk* psn_info_tracker = new psn_chunk(child_id, 0, "PSN_INFO_TRACKER");

				psn_info_tracker->add_child(new psn_chunk(PSN_INFO_TRACKER_NAME, new psn_name_data(), "PSN_INFO_TRACKER_NAME"));

				add_child(psn_info_tracker);
			}

			return psn_chunk::decode_child(child_id, buffer, buffer_size);
		}
	};

	class psn_decoder
	{
	public:

		psn_decoder(void) : last_frame_id_(0)
			, frame_packet_counter_(0)
		{
			// PSN_DATA 
			psn_data_packet_ = new psn_chunk(PSN_DATA_PACKET, 0, "PSN_DATA_PACKET");
			psn_data_packet_header_ = new psn_chunk(PSN_DATA_PACKET_HEADER, new ::psn::psn_packet_header_data(), "PSN_DATA_PACKET_HEADER");
			psn_data_tracker_list_ = new psn_data_tracker_list_chunk(PSN_DATA_TRACKER_LIST, 0, "PSN_DATA_TRACKER_LIST");
			// PSN_DATA_TRACKER chunks will be dynamically instantiated on decode_data 

			psn_data_packet_->add_child(psn_data_packet_header_);
			psn_data_packet_->add_child(psn_data_tracker_list_);

			// PSN_INFO
			psn_info_packet_ = new psn_chunk(PSN_INFO_PACKET, 0, "PSN_INFO_PACKET");
			psn_info_packet_header_ = new psn_chunk(PSN_INFO_PACKET_HEADER, new ::psn::psn_packet_header_data(), "PSN_INFO_PACKET_HEADER");
			psn_info_system_name_ = new psn_chunk(PSN_INFO_SYSTEM_NAME, new ::psn::psn_name_data(), "PSN_INFO_SYSTEM_NAME");
			psn_info_tracker_list_ = new psn_info_tracker_list_chunk(PSN_INFO_TRACKER_LIST, 0, "PSN_INFO_TRACKER_LIST");
			// PSN_INFO_TRACKER chunks will be dynamically instantiated on decode_info

			psn_info_packet_->add_child(psn_info_packet_header_);
			psn_info_packet_->add_child(psn_info_system_name_);
			psn_info_packet_->add_child(psn_info_tracker_list_);
		};
		~psn_decoder(void) 
		{
			delete psn_data_packet_;
			psn_data_packet_ = 0;

			delete psn_info_packet_;
			psn_info_packet_ = 0;
		};

		uint8_t get_last_decoded_frame_id(void) const
		{
			return last_frame_id_;
		};
		const psn_tracker_array& get_trackers(void) const
		{
			return trackers_;
		};

		bool get_psn_server_name(::std::string& server_name)
		{
			if (psn_info_system_name_ != 0)
			{
				::psn::psn_name_data* data = (::psn::psn_name_data*)psn_info_system_name_->get_data();

				if (data != 0)
				{
					server_name = data->name_;
					return true;
				}
			}

			return false;
		};

		bool decode(const ::std::string& packet)
		{
			const char* buffer = packet.c_str();
			size_t buffer_size = packet.length();

			if (buffer_size < SIZE_OF_PSN_CHUNK_HEADER)
				return false;

			psn_chunk_header chunk_header;

			// decode root chunk header
			memcpy(&chunk_header, buffer, SIZE_OF_PSN_CHUNK_HEADER);

			if (chunk_header.id_ == psn_data_packet_->get_id())
			{
				return decode_data_packet((void*)buffer, buffer_size);
			}
			else if (chunk_header.id_ == psn_info_packet_->get_id())
			{
				return decode_info_packet((void*)buffer, buffer_size);
			}

			return false;
		};

	private:

		bool decode_data_packet(void* buffer, size_t buffer_size)
		{
			// Remove all children from PSN_DATA_TRACKER_LIST chunk
			// It will add its own children during the decode process
			psn_data_tracker_list_->remove_all_children();

			if (!psn_data_packet_->decode(buffer, buffer_size))
				return false;

			bool last_packet = false;

			// Check if it's the last packet for this frame
			{
				psn_packet_header_data* data = (psn_packet_header_data*)psn_data_packet_header_->get_data();

				if (data != 0)
				{
					// Backup solution in case data->frame_packet_count_ is bad or we missed a packet.
					bool expect_new_frame = (frame_packet_counter_ == 0);

					if (data->frame_id_ != last_frame_id_ && !expect_new_frame)
					{
						frame_packet_counter_ = 0;

						trackers_ = frame_trackers_;
						frame_trackers_.clear();
					}

					last_frame_id_ = data->frame_id_;

					// Is it the last packet ?
					if (data->frame_packet_count_ == ++frame_packet_counter_)
					{
						last_packet = true; // Will copy frame_trackers_ to trackers_ at the end.
						frame_packet_counter_ = 0;
					}
				}
			}

			// Get all the PSN_DATA_TRACKER chunks and extract the trackers data from them 
			::std::vector< ::psn::psn_chunk* > trackers_chunk =
				psn_data_tracker_list_->get_children();

			for (size_t i = 0; i < trackers_chunk.size(); ++i)
			{
				psn_tracker t;
				t.id_ = trackers_chunk[i]->get_id();

				// Copy tracker info
				if (trackers_info_.find(t.id_) != trackers_info_.end())
					t.name_ = trackers_info_[t.id_];

				// Tracker Pos
				{
					::psn::psn_chunk* tracker_pos_chunk = trackers_chunk[i]->get_child_by_id(PSN_DATA_TRACKER_POS);
					if (tracker_pos_chunk != 0)
					{
						psn_vec3_data* pos_data = (psn_vec3_data*)tracker_pos_chunk->get_data();

						if (pos_data != 0)
						{
							t.pos_.x = pos_data->vec_.x;
							t.pos_.y = pos_data->vec_.y;
							t.pos_.z = pos_data->vec_.z;
						}
					}
				}

				// Tracker Speed
				{
					::psn::psn_chunk* tracker_speed_chunk = trackers_chunk[i]->get_child_by_id(PSN_DATA_TRACKER_SPEED);
					if (tracker_speed_chunk != 0)
					{
						psn_vec3_data* speed_data = (psn_vec3_data*)tracker_speed_chunk->get_data();

						if (speed_data != 0)
						{
							t.speed_.x = speed_data->vec_.x;
							t.speed_.y = speed_data->vec_.y;
							t.speed_.z = speed_data->vec_.z;
						}
					}
				}

				// Tracker Ori
				{
					::psn::psn_chunk* tracker_ori_chunk = trackers_chunk[i]->get_child_by_id(PSN_DATA_TRACKER_ORI);
					if (tracker_ori_chunk != 0)
					{
						psn_vec3_data* ori_data = (psn_vec3_data*)tracker_ori_chunk->get_data();

						if (ori_data != 0)
						{
							t.ori_.x = ori_data->vec_.x;
							t.ori_.y = ori_data->vec_.y;
							t.ori_.z = ori_data->vec_.z;
						}
					}
				}

				// Tracker status
				{
					::psn::psn_chunk* tracker_status_chunk = trackers_chunk[i]->get_child_by_id(PSN_DATA_TRACKER_STATUS);
					if (tracker_status_chunk != 0)
					{
						psn_tracker_status_data* status_data = (psn_tracker_status_data*)tracker_status_chunk->get_data();

						if (status_data != 0)
						{
							t.validity_ = status_data->validity_;
						}
					}
				}

				frame_trackers_[t.id_] = t;
			}

			if (last_packet)
			{
				trackers_ = frame_trackers_;
				frame_trackers_.clear();
			}

			return true;
		};
		bool decode_info_packet(void* buffer, size_t buffer_size)
		{
			// Remove all children from PSN_INFO_TRACKER_LIST chunk
			// It will add its own children during the decode process
			psn_info_tracker_list_->remove_all_children();

			if (!psn_info_packet_->decode((void*)buffer, buffer_size))
				return false;

			// Get all the PSN_INFO_TRACKER chunks and extract the trackers name from them 
			::std::vector< ::psn::psn_chunk* > trackers_chunk =
				psn_info_tracker_list_->get_children();

			for (size_t i = 0; i < trackers_chunk.size(); ++i)
			{
				uint16_t tracker_id = trackers_chunk[i]->get_id();

				// Tracker Name
				{
					::psn::psn_chunk* tracker_name_chunk = trackers_chunk[i]->get_child_by_id(PSN_INFO_TRACKER_NAME);
					if (tracker_name_chunk != 0)
					{
						psn_name_data* name_data = (psn_name_data*)tracker_name_chunk->get_data();

						if (name_data != 0)
						{
							trackers_info_[tracker_id] = name_data->name_;
						}
					}
				}
			}

			return true;
		};

	private:

		psn_chunk* psn_data_packet_;
		psn_chunk* psn_data_packet_header_;
		psn_data_tracker_list_chunk* psn_data_tracker_list_;

		psn_chunk* psn_info_packet_;
		psn_chunk* psn_info_packet_header_;
		psn_chunk* psn_info_system_name_;
		psn_info_tracker_list_chunk* psn_info_tracker_list_;

	private:

		psn_tracker_array                        trackers_;
		psn_tracker_array                        frame_trackers_; // temporary trackers until frame is completely decoded.

		::std::map< uint16_t, ::std::string >   trackers_info_; // keep the trackers info in a map

		uint8_t                                  last_frame_id_;
		uint8_t                                  frame_packet_counter_;

	};


} // namespace psn




