#include "pch.h"
#include "CppUnitTest.h"
#include "TestPublisher.h"
#include "TestSubscriber.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"
#include <chrono>
#include <thread>
#include <iostream>
#include <memory>
#include <string>
#include <stdexcept>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MotionPipelineTest
{
	template<typename ... Args>
	std::string string_format(const std::string& format, Args ... args)
	{
		size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
		if (size <= 0) { throw std::runtime_error("Error during formatting."); }
		std::unique_ptr<char[]> buf(new char[size]);
		snprintf(buf.get(), size, format.c_str(), args ...);
		return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
	}

	TEST_CLASS(MotionPipelineTest)
	{
	public:

		TEST_METHOD(MessageBus_Debounce_Test)
		{
			try
			{
				const long long DEBOUNCE_INTERVAL = 1000000000 / 60; // wait for each message

				//generate test data
				std::vector<Message*>* sent_messages = new std::vector<Message*>();

				//move right finger on y-axis
				//create 60 packages to send
				for (double i = 0; i < 60; i++)
				{
					sent_messages->push_back(new CoordinateMessage("right_finger", 1, i + (double)10, i + (double)20, i + (double)30));
				}

				MessageBus* message_bus = new MessageBus();
				TestPublisher* test_publisher = new TestPublisher(message_bus, *sent_messages);

				TestSubscriber* test_subscriber = new TestSubscriber(message_bus,
					[&test_subscriber, DEBOUNCE_INTERVAL](Message* message) -> void {
						//Logger::WriteMessage(string_format("Received Messages: %d\r\n", test_subscriber->get_received_message_count()).c_str());
						std::this_thread::sleep_for(std::chrono::nanoseconds(DEBOUNCE_INTERVAL)); //wait for debounce interval
					}
				, 60, false);

				//start the message bus
				long long start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

				message_bus->run();

				long long end = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

				Logger::WriteMessage(string_format("MessageBus Runtime: %dms\r\n", end - start).c_str());
				std::vector<Message*>* received_messages = test_subscriber->get_received_messages();

				//check the count of received messages
				Assert::AreEqual(60, (int)received_messages->size());

				//cleanup
				delete test_publisher;
				delete test_subscriber;
				delete message_bus;
				delete received_messages;
				delete sent_messages;
			}
			catch (const std::exception& e)
			{
				Logger::WriteMessage(e.what());
				Assert::Fail();
			}
		}

		TEST_METHOD(MessageBus_Debounce_Long_Test)
		{
			try
			{
				const long long DEBOUNCE_INTERVAL = 100000000 / 60; // wait for each message
				long long start = 0;
				int number_of_messages = 1000000;

				//generate test data
				std::vector<Message*>* sent_messages = new std::vector<Message*>();

				//create 10000000 to be published
				for (double i = 0; i < number_of_messages; i++)
				{
					sent_messages->push_back(new CoordinateMessage("right_finger", 1, i + (double)10, i + (double)20, i + (double)30));
				}

				MessageBus* message_bus = new MessageBus();
				TestPublisher* test_publisher = new TestPublisher(message_bus, *sent_messages);
				int previous_run = 0;

				TestSubscriber* test_subscriber = new TestSubscriber(message_bus,
					[&test_subscriber, &message_bus, &start, &previous_run, DEBOUNCE_INTERVAL](Message* message) -> void {
						long long now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

						if (((now - start) / 1000) != previous_run)
						{
							Logger::WriteMessage(string_format("Messages in second %d: %d\r\n", previous_run, test_subscriber->get_received_message_count()).c_str());
							previous_run++;
						}

						std::this_thread::sleep_for(std::chrono::nanoseconds(DEBOUNCE_INTERVAL)); //wait for debounce interval

						if ((now - start) >= 300000) //check if the messagebus is running for 5 minutes
						{
							Logger::WriteMessage(string_format("Total sent messages: %d\r\n", test_subscriber->get_received_message_count()).c_str());
							message_bus->stop();
						}
					}
				, number_of_messages); //stop at max messages anyways

				//start the message bus
				start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

				message_bus->run();

				long long end = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

				Logger::WriteMessage(string_format("MessageBus Runtime: %dms\r\n", end - start).c_str());

				//cleanup
				delete test_publisher;
				delete test_subscriber;
				delete message_bus;
				delete sent_messages;
			}
			catch (const std::exception& e)
			{
				Logger::WriteMessage(e.what());
				Assert::Fail();
			}
		}

		TEST_METHOD(MessageBus_CorrectOrder_Test)
		{
			try
			{
				//save received messages
				std::vector<Message*>* received_messages = new std::vector<Message*>();

				//generate test data
				std::vector<Message*>* sent_messages = new std::vector<Message*>();

				//move right finger on y-axis
				sent_messages->push_back(new CoordinateMessage("right_finger", 1, 10, 20, 30));
				sent_messages->push_back(new CoordinateMessage("right_finger", 1, 10, 30, 30));
				sent_messages->push_back(new CoordinateMessage("right_finger", 1, 10, 40, 30));
				sent_messages->push_back(new CoordinateMessage("right_finger", 1, 10, 50, 30));
				sent_messages->push_back(new CoordinateMessage("right_finger", 1, 10, 60, 30));

				MessageBus* message_bus = new MessageBus();
				TestPublisher* test_publisher = new TestPublisher(message_bus, *sent_messages);

				TestSubscriber* test_subscriber = new TestSubscriber(message_bus, [message_bus, sent_messages, received_messages](Message* message) -> void {
					Logger::WriteMessage(message->get_label().c_str());
					received_messages->push_back(message);

					if (received_messages->size() == 5)
					{
						message_bus->stop();
					}
					});

				//start the message bus
				message_bus->run();

				//check the count of received messages
				Assert::AreEqual(sent_messages->size(), received_messages->size());

				//check if the elements are in the same order and the same
				int sent_message_index = 0;
				while (sent_message_index < sent_messages->size())
				{
					Assert::AreEqual((int)sent_messages->at(sent_message_index), (int)received_messages->at(sent_message_index));
					sent_message_index++;
				}

				//cleanup
				delete test_publisher;
				delete test_subscriber;
				delete message_bus;
				delete received_messages;
				delete sent_messages;
			}
			catch (const std::exception& e)
			{
				Logger::WriteMessage(e.what());
				Assert::Fail();
			}
		}
	};
}