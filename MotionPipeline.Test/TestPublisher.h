#pragma once

#include "../MotionPipeline.Core/Publisher.h"

class TestPublisher :
	public Publisher
{
private:
	std::vector<Message*> messages;
	int message_index = 0;

public:
	TestPublisher(MessageBus* message_bus, std::vector<Message*> given_messages) : Publisher(message_bus)
	{
		messages = given_messages;
	}

	virtual void update()
	{
		if (!messages.empty() && message_index < messages.size())
		{
			Message* message = messages.at(message_index);
			send(message);

			message_index++;
		}
	}
};
