#pragma once

#include "../MotionPipeline.Core/Subscriber.h"
#include "../MotionPipeline.Domain/CoordinateMessage.h"

class TestSubscriber :
	public Subscriber
{
private:
	std::function<void(Message*)> subscriber_func;
	std::vector<Message*>* received_messages = new std::vector<Message*>();
	int received_message_count = 0;
	int stop_bus_at_messages = 0;
	bool clone_message = true;

public:

	TestSubscriber(MessageBus* message_bus, std::function<void(Message*)> subscriber_function, int stop_bus_at_messages = 0, bool clone_message = true) : Subscriber(message_bus)
	{
		subscriber_func = subscriber_function;
		this->stop_bus_at_messages = stop_bus_at_messages;
		this->clone_message = clone_message;
	}

	virtual void on_notify(Message* message)
	{
		received_message_count++;

		CoordinateMessage* coordinateMessage = (CoordinateMessage*)message;

		if (clone_message)
		{
			if (coordinateMessage != nullptr)
			{
				Message* clonedMessage = new CoordinateMessage(coordinateMessage->get_label(),
					coordinateMessage->get_sender_id(),
					coordinateMessage->get_x(),
					coordinateMessage->get_y(),
					coordinateMessage->get_z());
				received_messages->push_back(clonedMessage);
			}
			else
			{
				Message* clonedMessage = new Message(message->get_label(), message->get_sender_id());
				received_messages->push_back(clonedMessage);
			}
		}
		else
		{
			received_messages->push_back(message);
		}

		if (subscriber_func != nullptr)
		{
			subscriber_func(message);
		}

		if (stop_bus_at_messages == received_message_count)
		{
			message_bus->stop();
		}
	}

	int get_received_message_count()
	{
		return received_message_count;
	}

	std::vector<Message*>* get_received_messages()
	{
		return received_messages;
	}
};
